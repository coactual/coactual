<?php

namespace Coactual;

class Container implements \ArrayAccess
{
    private $container = array();
	private $factories;
    
    public function __construct(array $values = array())
    {
    	$this->factories = new \SplObjectStorage();

        foreach($values as $key => $value) {
            $this->offsetSet($key, $value);
        } 
    }

    public function offsetSet($offset, $value) 
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }    
    }

    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) 
    {
    	if (!is_object($this->container[$offset])
			|| !method_exists($this->container[$offset], '__invoke')
			) {
				return $this->container[$offset];
			}

		if (isset($this->factories[$this->container[$offset]])) {
			return $this->container[$offset]($this);
		}
		
		$val = $this->container[$offset] = $this->container[$offset]($this);
		
		return $val;
    }
	
	public function keys()
	{
		return array_keys($this->container);
	}
	
	public function factory($callable)
	{
		if (!is_object($callable) || !method_exists($callable, '__invoke')) {
            throw new \InvalidArgumentException('Factory must be an invokable object.');
        }
		
		$this->factories->attach($callable);
		
		return $callable;
	}
}