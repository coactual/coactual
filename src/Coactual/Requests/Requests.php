<?php
/**
 * $Header$
 *
 * @author Will Cliffe
 */
namespace Coactual\Requests;

class Requests
{
	const HTTP_GET = 'GET';
	const HTTP_POST = 'POST';

	public static function get($sUri, $aHeaders=array(), $aOptions=array())
	{
		return self::sendRequest($sUri, array(), self::HTTP_GET);
	}

	public function post($sUri, $aParams = array(), $aHeaders=array(), $aOptions=array())
	{
		return self::sendRequest($sUri, $aParams, self::HTTP_POST);
	}

	/**
	 * Send the request (POST or GET)
	 *
	 * @param string $sUri The URL resource
	 * @param array $aParams The array for POSTing data
	 * @param string $sMethod The HTTP Method used, default GET
	 * @return string Response string from request
	 */
	private static function sendRequest($sUri, $aParams=array(), $sMethod=self::HTTP_GET, $aHeaders=array())
	{
		// Build the http query for POST variables
		if (!empty($aParams)) {
			$sQuery = http_build_query($aParams);
		} else {
			$sQuery = '';
		}
		$sHeader = '';
		foreach ($aHeaders as $sKey => $sValue) {
			$sHeader .= "{$sKey}: {$sValue};";
		}
		if (!$sHeader) { 
			$sHeader = 'Content-type: application/json;';
		}
		// Set up the options array for context creation
		$aOptions = array(
			'http' => array(
				'method' => $sMethod,
				'header' => $sHeader . 'Content-length: ' . strlen($sQuery).'\r\n',
				'timeout' => 3,
				'content' => $sQuery,
			)
		);

		// Create the stream context
		$hContext = stream_context_create($aOptions);
		// Get the response from the stream
		$sResponse = file_get_contents($sUri, false, $hContext);

		return $sResponse;
	}
}