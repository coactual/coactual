<?php

namespace Coactual;

class Request
{
	public function __construct($request=array())
	{
		if(empty($request)) {
			$request = array(
				'route' => $this->getVar(array('REQUEST_URI', 'DOCUMENT_URI'), ''),
				'params' => $this->parseQuery($this->getVar('QUERY_PARAMS', '')),
				'headers' => $this->parseHeaders()
			);
		}
		
		foreach($request as $key => $value) {
			$this->$key = $value;
		}
	}
	
	
	public function getVar($var, $default)
	{
        if(is_array($var)) {
            foreach($var as $check) {
                if(isset($_SERVER[$check])) {
                    return $_SERVER[$check];
                }   
            }
        } else if (isset($_SERVER[$var])) {
            return $_SERVER[$var];
		}
		
		return $default;
	}

	private function parseQuery($var)
	{
		$query = Array();
		if(empty($var)) {
			return $query;
		}
		$params = explode('&', $var);
		foreach($params as $param)
		{
			if(stripos($param, '=')) {
				list($k, $v) = explode('=', $param);
				$query[$k] = $v;

			} else {
				$query[$param] = '';
			}
		}
		return $query;
	}

	private function parseHeaders()
	{
		$headers = Array();
		$headers['CONTENT_TYPE'] = isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : '';
		$headers['CONTENT_LENGTH'] = isset($_SERVER['CONTENT_LENGTH']) ? $_SERVER['CONTENT_LENGTH'] : 0;

		return $headers;
	}
	
}
