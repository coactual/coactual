<?php

namespace Coactual;

class Post
{
    public $errors = Array();

    private $aProperties = array();

    public function __construct($request, $options = array())
    {
        $required = isset($options['required']) ? $options['required'] : array();
        if(isset($request->headers['CONTENT_TYPE']) && $request->headers['CONTENT_TYPE'] == 'application/json') {
            $requests = json_decode(file_get_contents("php://input"));
        }
        else 
        {
            $requests = array_merge($_POST, $_REQUEST);
        }
        if(empty($requests)) {
            foreach($required as $var)
            {
                $this->errors[$var] = true;
            }

        } else {
            foreach($requests as $arg => $value)
            {
                if(in_array($arg, $required)) {
                    unset($required[array_search($arg, $required)]);
                }
                $this->aProperties[$arg] = $value;
            }

            foreach($required as $key) 
            {
                $this->errors[$key] = true;
            }
        }
    }


    public function get($var)
    {
        if(in_array($var, $this->errors)) {
            return null;
        }

        if (!in_array($var, array_keys($this->aProperties))) {
            return null;
        }
        return $this->aProperties[$var];
    }

    public function set($field, $value)
    {
        $this->aProperties[$field] = $value;
    }

}
