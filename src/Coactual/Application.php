<?php

namespace Coactual;

use Coactual\Container;
use Coactual\Controller\ControllerBag;
use Coactual\Controller\Controller;
use Coactual\Router\Routes;
use Coactual\Router\Route;

class Application extends Container
{
	
    public function __construct(array $values=array())
    {
        parent::__construct();
        
		$app = $this;
		
		$this['routes'] = function () use ($app) {
			 return $app['routes_factory'];
		};
		
		$this['routes_factory'] = $this->factory(function() use ($app) {
			return new Routes();
		});
		
		foreach($values as $key => $value) {
			$this[$key] = $value;
		}
    }
	
	public function get($path, $callback)
	{
		$func = new \ReflectionFunction($callback);
		$vars = $func->getStaticVariables();
		$options = isset($vars['options']) ? $vars['options'] : array();
		// Remove trailing '/'
		if (strlen($path) > 1) {
			$path = rtrim($path, '/');
		}
		return $this['routes']->add($path, $callback, $options);
	}
	
	public function run()
	{
		$request = new Request();
		$this['request'] = $request;
		$this['args'] = new Post($this['request'], $this['routes']->getOpts($request->route));

		$this['routes']->get($request->route);
	}
	
}
