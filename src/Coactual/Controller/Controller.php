<?php

namespace Coactual\Controller;

use Coactual\API\ControllerInterface;

class Controller implements ControllerInterface
{
	/* @var string $name */
	protected $name;
	
	/* @var mixed bag */
	protected $bag;
	
	public function __construct($name=false, $closure=null)
	{
		if(!$name) {
			throw new \InvalidArgumentException('Missing name field for controller.');
		}
		
		$this->setName($name);
		$this->setBag($closure);
	}
	
	public function register()
	{
		
	}
	
	public function unregister()
	{
		
	}
	
	public function setName($name='')
	{
		$this->name = $name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setBag($bag=null)
	{
		if(is_null($bag)) {
			throw new \InvalidArgumentException("Missing valid function|closure|data-bag");
		}
		
		$this->bag = $bag;
	}
	
	public function getBag()
	{
		return $this->bag;
	}
	
	public function activate()
	{
		if(is_object($this->bag) || is_callable($this->bag)) {
			return $this->bag($this);
		}
	}
}
