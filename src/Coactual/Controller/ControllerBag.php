<?php

namespace Coactual\Controller;

class ControllerBag 
{
	public $controllers = array();
	
	public function __construct(array $controllers=array())
	{
		
	}
	
	public function add(Controller $controller) 
	{
		print_r($controller->getName());
		$this->controllers[$controller->getName()] = $controller;
		print_r($this->controllers);
	}
	
	public function remove(Controller $controller)
	{
		unset($this->controllers[$controller->getName()]);
	}
	
}
