<?php

namespace Coactual\Database;

class Database
{
	public $PDOStatement = null;

	private $oConnection = null;

	public function __construct($config=array())
	{
		$dsn = "{$config['driver']}:dbname={$config['database']};host={$config['host']}";
		$this->oConnection = new \PDO($dsn, $config['user'], $config['password']);

	}

	public function prepare($sql, $params=array())
	{
		$this->PDOStatement = $this->oConnection->prepare($sql);

		foreach($params as $key => $value) {
			$this->PDOStatement->bindParam($key, $value);
		}

		return $this->PDOStatement;
	}


}