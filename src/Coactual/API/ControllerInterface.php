<?php

namespace Coactual\API;

interface ControllerInterface
{
	public function register();
	public function unregister();
}
