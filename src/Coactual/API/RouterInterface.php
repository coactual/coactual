<?php

namespace Coactual\API;

interface RouterInterface
{
    public function get($route, $closure);
}
