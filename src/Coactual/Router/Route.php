<?php

namespace Coactual\Router;

use Coactual\Container;

class Route extends Container
{
	/* @var string path */
	public $path;
	
	
	public function __construct($path = '/', $callback = null)
	{
		$app = $this;
		
		$this->path = $path;
		$this['callback'] = $callback;
	}
	
	public function run()
	{
		return $this['callback']();
	}
	
}
