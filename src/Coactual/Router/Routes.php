<?php

namespace Coactual\Router;

use Coactual\Router\Route;
use Coactual\Container;	

class Routes implements \IteratorAggregate, \Countable
{
	public $routes = array();
	public $options = array();
	
	public function __clone()
    {
        foreach ($this->routes as $name => $route) {
            $this->routes[$name] = clone $route;
        }
    }
		
	public function add($path, $callback=null, $options=array())
	{
		$this->routes[$path] = $callback;
		$this->options[$path] = $options;
	}
	
	public function get($path)
	{
		$path = $this->parsePath($path);
		// Remove any trailing '/'
		if (strlen($path) > 1) {
			$path = rtrim($path, '/');
		}
		if(isset($this->routes[$path])) {
			return $this->routes[$path]();
		} else {
			header('HTTP/1.0 404 Not Found');
			return false;
		}
	}

	private function parsePath($path='')
	{
		return preg_replace('/\?.*/i', '', $path);
	}

	public function getOpts($path)
	{
		return isset($this->options[$path]) ? $this->options[$path] : array();
	}
	
	public function getIterator()
	{
		return new \ArrayIterator($this->routes);
	}
	
	public function count()
	{
		return count($this->routes);
	}
}
